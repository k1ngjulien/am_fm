function y = ease_in_out(x)
    if x <= 0.5
        y = 2 *x *x;
    else
        x = x - 0.5;
        y = 2 *x * (1 - x)+ 0.5;
    end
end