/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "dma.h"
#include "i2s.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdbool.h>
#include "../cs43l22/cs43l22.h"
#include "lsm303agr.h"
#include "stm32f411e_disco_bus.h"
#include "motion_tl2.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
// float pi constant
#define M_PIf		3.14159265358979323846f

#define DAC_I2C_ADDR 0x94

// alternating int16 for left and right channel
#define OUT_BUF_SIZE 4096
int16_t outputBuffer[OUT_BUF_SIZE];

bool buttonRisingEdge();
void initAccelerometer();
void initMotionTL();
void updateAngles();
void calculateSamples(int16_t *buffer, size_t len);

LSM303AGR_ACC_Object_t accObj = {
        .acc_odr = LSM303AGR_XL_ODR_100Hz,
};

LSM303AGR_IO_t accIO = {
        .BusType = LSM303AGR_I2C_BUS, // i2c
        .Delay = (LSM303AGR_Delay_Func) HAL_Delay,
        .ReadReg = BSP_I2C1_ReadReg,
        .WriteReg = BSP_I2C1_WriteReg,
        .Address = LSM303AGR_I2C_ADD_XL,
        .Init = BSP_I2C1_Init,
        .DeInit = BSP_I2C1_DeInit,
};

float rollAngle = 0;
float pitchAngle = 0;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2S3_Init();
  MX_CRC_Init();
  /* USER CODE BEGIN 2 */

  initAccelerometer();
  initMotionTL();

  // initialize DAC
  // volume 85 is the max before output sinewave starts clipping
  cs43l22_Init(DAC_I2C_ADDR, OUTPUT_DEVICE_HEADPHONE, 85, AUDIO_FREQUENCY_48K);

  // start DAC
  cs43l22_Play(DAC_I2C_ADDR, outputBuffer, OUT_BUF_SIZE);

  // initialize buffer with samples
  calculateSamples(outputBuffer, OUT_BUF_SIZE);

  // start DMA in circular mode
  HAL_I2S_Transmit_DMA(&hi2s3, (uint16_t *)outputBuffer, OUT_BUF_SIZE);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  bool mute = false;
  while (1)
  {
    // mute button
    if (buttonRisingEdge()) {
      mute = !mute;
      cs43l22_SetMute(DAC_I2C_ADDR, mute);
    }

    updateAngles();
    HAL_Delay(1);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void initAccelerometer() {
  uint8_t id = 0;

  LSM303AGR_ACC_RegisterBusIO(&accObj, &accIO);
  LSM303AGR_ACC_Init(&accObj);
  LSM303AGR_ACC_Enable(&accObj);

  LSM303AGR_ACC_ReadID(&accObj, &id);

  if(id != LSM303AGR_ID_XL)
    while(1) {} // error
}

void initMotionTL() {
  MotionTL2_Init(MTL2_MCU_STM32);

  MTL2_knobs_t knobs = {};
  MotionTL2_GetKnobs(&knobs);

  knobs.mode = MTL2_DUAL_PLANE; // x and y angle measurement
  knobs.orn[0] = 's'; // sensor orientation
  knobs.orn[1] = 'w';
  knobs.fullscale = 4.0f; // sensor max g
  knobs.k = 50.0f;  // filter coefficient, bigger = more smoothing
  MotionTL2_SetKnobs(&knobs);
}

void updateAngles() {
  LSM303AGR_Axes_t data; // data in milli g
  LSM303AGR_ACC_GetAxes(&accObj, &data);

  MTL2_input_t dataIn = {
          .acc_x = (float)data.x * 0.001f,
          .acc_y = (float)data.y * 0.001f,
  };
  MTL2_output_t dataOut;

  MotionTL2_Update(&dataIn, HAL_GetTick(), &dataOut);

  pitchAngle = dataOut.psi_2x;
  rollAngle = dataOut.theta_2x;
}


void calculateSamples(int16_t *buffer, size_t len) {
  // sampling time/frequency
  const float fs = 48000.0f;
  const float Ts = 1.0f/fs;
  // base signal
  const float fsig0 = 500.0f;
  const float Asig0 = 32000.0f;
  // modulation amplitude [Hz]
  const float Amodulation = 200.0f;
  // signal phase
  static float phi = 0.0f;

  for(size_t i=0; i<len; i+=2)
  {
    // silent to full volume
    float Asig = Asig0 * (pitchAngle+90.0f)/180.f;
    // fsig +/- Amodulation Hz
    float fsig = fsig0 + Amodulation * (rollAngle/90.f);

    // update signal phase
    // phi[k] = phi[k-1] + 2pi*fsig*Ts
    phi += 2.0f*M_PIf*fsig*Ts;

    // limit phase in range 0..2pi
    if (phi > 2.0f*M_PIf) {
      phi -= 2.0f*M_PIf;
    }

    // calculate sample y[k]
    float sample = Asig * sinf(phi);

    // output sample (left and right channel)
    buffer[i] = (int16_t)sample;
    buffer[i + 1] = (int16_t)sample;
  }
}

void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hi2s)
{
  // upper half sent
  // update upper buffer
  int16_t *start = outputBuffer + OUT_BUF_SIZE / 2;
  calculateSamples(start, OUT_BUF_SIZE / 2);
}


void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
{
  // lower half sent
  // update lower buffer
  calculateSamples(outputBuffer, OUT_BUF_SIZE / 2);
}

bool buttonRisingEdge() {
  static bool last = false;

  bool current = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == GPIO_PIN_SET;
  bool rising = (current == true && last == false);
  last = current;

  return rising;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
