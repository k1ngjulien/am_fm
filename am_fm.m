% Simulation amplituden/frequenzmodulation auf basis von winkel
% Julian Kandlhofer, Sayedullah Mosawi, Patrik Schwarzenecker

fs = 48000;
Ts = 1/fs;

fsin = 500;
Asin = 0.5;

Tges =  10;

NumSamples = Tges/Ts;

t = 0:Ts:Tges-Ts;

y = zeros(1,length(t));

winkelMin = -90;
winkelMax = 90;

winkelStep = (winkelMax - winkelMin)/NumSamples;

winkelX = winkelMin:winkelStep:winkelMax-winkelStep;
% winkelX = sin(2* 2*pi*t) * winkelMax;

% winkelX = winkelMin:winkelStep:winkelMax-winkelStep;
winkelY = zeros(1,length(t));

f0 = 1000;
fmax = 5000;
fmin = 100;

A0 = 0.5;
Amax = 1;
Amin = 0.1;

phi = zeros(1,length(t) +1);

for k = 1:1:length(y)
    % frequenzmodulation
    winkelXSample = winkelX(k);
    if winkelXSample > 0
        fsin = f0 + (fmax - f0)* (winkelXSample/winkelMax);
    else
        fsin = f0 - (f0 - fmin)* abs(winkelXSample)/abs(winkelMin);
    end

    % amplitudenmodulation
    winkelYSample = winkelY(k);
    if winkelYSample > 0
        Asin = A0 + (Amax - A0)* winkelYSample/winkelMax;
    else
        Asin = A0 - (A0 - Amin)* abs(winkelYSample)/abs(winkelMin);
    end

    phi(k+1) = phi(k) + 2*pi*fsin*Ts;

    % schutz vor überlauf/ungenauigkeit bei hohen phi
    % if phi(k+1) >= 2*pi
    %     phi(k+1) = phi(k+1)-2*pi;
    % end

    y(k) = Asin * sin(phi(k+1));
end

figure(1)
plot(t, y)
figure(2)
plot(t, diff(phi))
