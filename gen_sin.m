function y = gen_sin(A, t, phi)

    y = A*sin(t + phi);
end